const express = require('express')
const app = express()

app.get('/', (req, res) => res.send('ok'))


app.get('/api', (req, res) => {
    res.status(200).json({status: 'ok', message: 'api ready'})
})


app.listen(3000, () => console.log('Server ready'))

